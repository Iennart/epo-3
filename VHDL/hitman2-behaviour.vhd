library IEEE;
use IEEE.std_logic_1164.ALL;

architecture behaviour of hitman2 is

type hit_state is (reset_state, first_red, first_blue, second_red, second_blue);
signal current_state, next_state :hit_state := reset_state;

begin

P1: process(clk, reset)
begin
if (clk'event and clk = '1') then
	if (reset = '1') then
		start <= '0';
		current_state <= reset_state;
	else
		current_state <= next_state;
		
		if (started = '0') then
			start <= '1';
		else
			start <= '0';
		end if;
	end if;
end if;
end process;

P2: process(current_state, x_red, y_red, x_blue, y_blue, y0, y1, type0, type1)
begin
	case current_state is
		when first_red =>
			x_circle <= x_red;
			y_circle <= y_red;
			block_type <= type0;
			y_block <= y0;

		when first_blue =>
			x_circle <= x_blue;
			y_circle <= y_blue;
			block_type <= type0;
			y_block <= y0;

		when second_red =>
			x_circle <= x_red;
			y_circle <= y_red;
			block_type <= type1;
			y_block <= y1;

		when second_blue =>
			x_circle <= x_blue;
			y_circle <= y_blue;
			block_type <= type1;
			y_block <= y1;
		
		when reset_state =>
			x_circle <= "0000000";
			y_circle <= "00000000";
			block_type <= "000";
			y_block <= "00000000";
	end case;
end process;

P3: process(done)
begin
if (done'event and done = '1') then
	case current_state is
		when first_red =>
			next_state <= first_blue;

		when first_blue =>
			next_state <= second_red;

		when second_red =>
			next_state <= second_blue;

		when second_blue =>
			next_state <= reset_state;
		
		when reset_state =>
			next_state <= first_red;
	end case;
end if;
end process;

end behaviour;
