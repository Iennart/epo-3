library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

architecture behaviour of vga_core is

	signal con_col : std_logic_vector(7 downto 0);
	signal con_row : std_logic_vector (6 downto 0);
	signal color_type: std_logic_vector (1 downto 0);

begin
VIDEO_DISPLAY_DATA : process (clk)

begin
	if clk'event and clk = '1' then
		rom_mux_output <= rom_data ((CONV_INTEGER(con_row(2 downto 0)))); -- send correct bit of the word to the output
		color <= color_type;
		if row_address >= x_b1 and row_address < x2_b1 and col_address > y_b1 and col_address <= y2_b1 then -- print 1st ball
			con_col <= col_address - y_b1; --follows y coordinate to send the correct bit of the word to output
			con_row <= row_address - x_b1; --follows x coordinate to select the correct address of the rom

			rom_address <= ("0" & con_col(3 downto 0)); --ball shape is located in address 0-7 so only the lowest 3 bits are relevant
			color_type <= "01";
		elsif row_address >= x_b2 and row_address < x2_b2 and col_address > y_b2 and col_address <= y2_b2 then -- print 2nd ball
			con_col <= col_address - y_b2;
			con_row <= row_address - x_b2;

			rom_address <= ("0" & con_col(3 downto 0));
			color_type <= "10";
		elsif t_o1(2) = '0' and row_address >= "0010100" and row_address <= "1000001" and col_address >= y_o1 and col_address <= y2_o1 then --print 1st bar (right side)
			-- These two signals are only relevant if an actual character is drawn to read the correct address/word.
			-- The bars are fully white however, so specifying these signals is unnecessary (this may change later if the design of the bar is changed)
			con_col <= "00000000";
			con_row <= "0000000";

			rom_address <= "01001";	-- 9
			color_type <= "00";
		elsif t_o1(2) = '1' and row_address >= "0110111" and row_address <= "1100011" and col_address >= y_o1 and col_address <= y2_o1 then --(left side)
			con_col <= "00000000";
			con_row <= "0000000";

			rom_address <= "01001";	-- 9
			color_type <= "00";
		elsif t_o2(2) = '0' and row_address >= "0010100" and row_address <= "1000001" and col_address >= y_o2 and col_address <= y2_o2 then --2nd bar
			con_col <= "00000000";
			con_row <= "0000000";

			rom_address <= "01001";	-- 9
			color_type <= "00";
		elsif t_o2(2) = '1' and row_address >= "0110111" and row_address <= "1100011" and col_address >= y_o2 and col_address <= y2_o2 then
			con_col <= "00000000";
			con_row <= "0000000";

			rom_address <= "01001";	-- 9
			color_type <= "00";
		elsif t_o3(2) = '0'  and row_address >= "0010100" and row_address <= "1000001" and col_address >= y_o3 and col_address <= y2_o3 then --3rd bar
			con_col <= "00000000";
			con_row <= "0000000";

			rom_address <= "01001";	-- 9
			color_type <= "00";
		elsif t_o3(2) = '1' and row_address >= "0110111" and row_address <= "1100011" and col_address >= y_o3 and col_address <= y2_o3 then
			con_col <= "00000000";
			con_row <= "0000000";

			rom_address <= "01001";	-- 9
			color_type <= "00";
		elsif t_o4(2) = '0' and row_address >= "0010100" and row_address <= "1000001" and col_address >= y_o4 and col_address <= y2_o4 then --4th bar
			con_col <= "00000000";
			con_row <= "0000000";

			rom_address <= "01001";	-- 9
			color_type <= "00";
		elsif t_o4(2) = '1' and row_address >= "0110111" and row_address <= "1100011" and col_address >= y_o4 and col_address <= y2_o4 then
			con_col <= "00000000";
			con_row <= "0000000";

			rom_address <= "01001";	-- 9
			color_type <= "00";
		else
			con_col <= "00000000";
			con_row <= "0000000";
			
			-- The screen is black except for the bars and balls so the default address is 01001 which corresponds to an 8bit vector of zeroes
			rom_address <= "01010";	-- 10
			color_type <= "00";
		end if;

	end if;
end process VIDEO_DISPLAY_DATA;

end behaviour;
