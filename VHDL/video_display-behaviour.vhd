library IEEE;
use IEEE.std_logic_1164.all;

architecture behaviour of video_display is
begin
video_display : process (clk) --alle signalen om blokken en pixelposities bij te houden
begin
	if clk'event and clk = '1' then
		col_address <= column;
		row_address <= row(8 downto 2);
	end if;
end process video_display;
end behaviour;