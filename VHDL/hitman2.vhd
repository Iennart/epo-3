library IEEE;
use IEEE.std_logic_1164.ALL;

entity hitman2 is
   port(clk      :in    std_logic;
        reset    :in    std_logic;
        x_red    :in    std_logic_vector(6 downto 0);
        x_blue   :in    std_logic_vector(6 downto 0);
        y_red    :in    std_logic_vector(7 downto 0);
        y_blue   :in    std_logic_vector(7 downto 0);
        --x00    :in    std_logic_vector(8 downto 0);
        --x01    :in    std_logic_vector(8 downto 0);
        --x10    :in    std_logic_vector(8 downto 0);
        --x11    :in    std_logic_vector(8 downto 0);
				type0    :in    std_logic_vector(2 downto 0);
				type1    :in    std_logic_vector(2 downto 0);
        height   :in    std_logic_vector(5 downto 0);
        y0       :in    std_logic_vector(7 downto 0);
        y1       :in    std_logic_vector(7 downto 0);
				done     :in    std_logic;
				started  :in    std_logic;
				start    :out   std_logic;
				block_type :out std_logic_vector(2 downto 0);
				--x_block_0, x_block_1 :out std_logic_vector(6 downto 0) := "0000000";
				x_circle :out std_logic_vector(6 downto 0) := "0000000";
				y_circle, y_block              :out std_logic_vector(7 downto 0) := "00000000"
);
end hitman2;























