configuration gotygolddeluxe_synthesised_cfg of gotygolddeluxe is
   for synthesised
      for all: hitman2 use configuration work.hitman2_behaviour_cfg;
      end for;
      for all: hit_math_unit use configuration work.hit_math_unit_behaviour_cfg;
      end for;
      for all: subtraction use configuration work.subtraction_synthesised_cfg;
      end for;
      for all: decoder use configuration work.decoder_behaviour_cfg;
      end for;
      for all: controller use configuration work.controller_behaviour_cfg;
      end for;
      for all: coordcalc use configuration work.coordcalc_behaviour_cfg;
      end for;
      for all: typebuilder use configuration work.typebuilder_behavioural_cfg;
      end for;
      for all: counter use configuration work.counter_behavioural_cfg;
      end for;
      for all: bar use configuration work.bar_behaviour_cfg;
      end for;
   end for;
end gotygolddeluxe_synthesised_cfg;


