library ieee;
use ieee.std_logic_1164.all;

architecture behavioral of rom is
  type mem is array ( 0 to 2**4 - 1) of std_logic_vector(7 downto 0);
  constant my_rom : mem := (
    0  => "00111100", --balletjesvorm (zonder kleur nog)
    1  => "01111110",
    2  => "11111111",
    3  => "11111111",
    4  => "11111111",
    5  => "11111111",
    6  => "01111110",
    7  => "00111100",
    8  => "00000000",
    9  => "11111111", --zwarte balk 
    10 => "00000000", --witte balk
    11 => "11111111",
    12 => "11111111",
    13 => "11111111",
    14 => "11111111",
    15 => "11111111");
begin
   process (address)
   begin
     case address is
       when "00000" => q <= my_rom(0);
       when "00001" => q <= my_rom(1);
       when "00010" => q <= my_rom(2);
       when "00011" => q <= my_rom(3);
       when "00100" => q <= my_rom(4);
       when "00101" => q <= my_rom(5);
       when "00110" => q <= my_rom(6);
       when "00111" => q <= my_rom(7);
       when "01000" => q <= my_rom(8);
       when "01001" => q <= my_rom(9);
       when "01010" => q <= my_rom(10);
       when "01011" => q <= my_rom(11);
       when "01100" => q <= my_rom(12);
       when "01101" => q <= my_rom(13);
       when "01110" => q <= my_rom(14);
       when "01111" => q <= my_rom(15);
       when others => q <= "00000000";
     end case;
  end process;
end architecture behavioral;

