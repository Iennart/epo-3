library IEEE;
use IEEE.std_logic_1164.ALL;

entity ultron is
   port(clk   :in    std_logic;
        reset :in    std_logic;
        left  :in    std_logic;
        right :in    std_logic;
        x_red :out   std_logic_vector(6 downto 0);
        y_red :out   std_logic_vector(7 downto 0);
        x_blue:out   std_logic_vector(6 downto 0);
        y_blue:out   std_logic_vector(7 downto 0));
end ultron;


